import { NextResponse } from "next/server";

export type HttpStatusCode = 200 | 201 | 202 | 203 | 400 | 403 | 404 | 500;
export type Error = "INVALID_REQUEST" | "VALUE_NOT_FOUND";

export type RallxResponse<Data = any> = {
  status?: HttpStatusCode;
  message?: string;
  error?: Error;
  data?: Data;
};
export type SendResponse = (params: RallxResponse) => void;

export const sendResponse: SendResponse = (params) => {
  const { status, ...rest } = params || {};
  const statusCode = status ? status : rest?.error ? 400 : 200;

  return NextResponse.json(rest, {
    status: statusCode,
  });
};
