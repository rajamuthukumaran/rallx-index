export const validate = (data: Record<string, any>, required: string[]) => {
  return required.reduce((acc, key) => {
    if (acc) {
      if (data[key]) return acc;
      return false;
    }

    return acc;
  }, true);
};
