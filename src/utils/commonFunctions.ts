export const logError = (err: any, onlyOnDev?: boolean) => {
  if (onlyOnDev) {
    console.error(err);
  }
};
