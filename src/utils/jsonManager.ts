import path from "path";
import { promises as fs } from "fs";
import { isEmpty } from "lodash";
import { DataFiles } from "../../data/type";

export const saveData = (file: DataFiles, data: Record<string, any>) => {
  if (!isEmpty(data)) {
    const dataDir = path.join(process.cwd(), "data");
    fs.writeFile(`${dataDir}/${file}.json`, JSON.stringify(data));
  }
};

export const readData = async (file: DataFiles) => {
  const dataDir = path.join(process.cwd(), "data");
  const data = await fs.readFile(`${dataDir}/${file}.json`, "utf-8");
  return JSON.parse(data);
};
