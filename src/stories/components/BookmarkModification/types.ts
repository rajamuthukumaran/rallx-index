import { UseFieldArrayReturn, UseFormReturn } from "react-hook-form";
import { Dispatch } from "react";
import { Bookmark } from "../../../../data/type";

export type BookmarkFormValue = {
  bookmarks: Bookmark[];
};

export type BookmarkModificationProps = {
  isDark?: boolean;
  onSubmit?: (data: Bookmark[]) => void;
  bookmarks?: Bookmark[];
};

export type Editing = null | {
  index: number;
  data?: Bookmark;
};

export type BookmarkModificationFormProps = {
  formObj: UseFormReturn<BookmarkFormValue, any, undefined>;
  formArray: UseFieldArrayReturn<BookmarkFormValue, "bookmarks", "id">;
  onSubmit: (values: BookmarkFormValue) => Promise<void> | void;
  editing: Editing;
  setEditing: Dispatch<React.SetStateAction<Editing>>;
};
