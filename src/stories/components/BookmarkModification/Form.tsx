/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback } from "react";
import clsx from "clsx";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FormInput, FormRadioButton } from "../../form";
import Button from "../../inputs/Button";
import SilentButton from "../../inputs/SilentButton";
import { BookmarkModificationFormProps } from "./types";

const Form: React.FC<BookmarkModificationFormProps> = ({
  formArray,
  formObj,
  onSubmit,
  editing,
  setEditing,
}) => {
  const {
    control,
    handleSubmit,
    watch,
    getValues,
    trigger,
    formState: { isValid },
  } = formObj;
  const { fields, append, remove } = formArray;

  const getFieldName = useCallback((name: string) => {
    return watch(name as any);
  }, []);

  const editBookmark = useCallback((index: number) => {
    const data = getValues(`bookmarks.${index}`);
    setEditing({
      index,
      data,
    });
  }, []);

  const addNewBookmark = useCallback(() => {
    append({
      iconType: "custom",
      name: "",
      link: "",
      background: "",
      icon: "",
    });
    setEditing({
      index: fields?.length,
    });
  }, [fields?.length]);

  const handleSave = useCallback(() => {
    trigger();
    isValid && setEditing(null);
  }, [isValid]);

  const handleDelete = useCallback(() => {
    if (editing !== null) {
      remove(editing.index);
      setEditing(null);
    }
  }, [editing]);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        {fields.map((field, index) => {
          const fieldPrefix = `bookmarks.${index}`;
          const fieldName = getFieldName(`${fieldPrefix}.name`);

          if (editing?.index !== index) {
            return (
              <div
                className={clsx(
                  "p-5 bg-gray-600 rounded-xl",
                  "flex justify-between my-4"
                )}
                key={field.id}
              >
                {fieldName}
                <SilentButton
                  disabled={!isValid}
                  icon="pen"
                  name="Edit"
                  size="md"
                  isDark
                  onClick={() => editBookmark(index)}
                />
              </div>
            );
          }

          return (
            <div
              key={field.id}
              className={clsx(
                "grid gap-5 md:grid-cols-2",
                "p-5 bg-gray-600 rounded-xl"
              )}
            >
              <FormInput
                name={`${fieldPrefix}.name`}
                control={control}
                fieldProps={{
                  isDark: true,
                  label: "Name",
                }}
                required
              />
              <FormInput
                name={`${fieldPrefix}.link`}
                control={control}
                fieldProps={{
                  isDark: true,
                  label: "Link",
                }}
                required
              />
              <FormInput
                name={`${fieldPrefix}.icon`}
                control={control}
                fieldProps={{
                  isDark: true,
                  label: "Icon",
                }}
              />
              <FormInput
                name={`${fieldPrefix}.background`}
                control={control}
                fieldProps={{
                  isDark: true,
                  label: "Background Color",
                }}
              />
              <FormRadioButton
                control={control}
                name={`${fieldPrefix}.iconType`}
                fieldProps={{
                  values: [
                    {
                      label: "Custom",
                      value: "custom",
                    },
                    {
                      label: "Fontawsome",
                      value: "fontawesome",
                    },
                  ],
                }}
              />

              <div className="flex md:justify-end justify-between">
                <Button
                  className="mr-4"
                  colorType="Error"
                  onClick={handleDelete}
                >
                  Delete
                </Button>
                <Button colorType="Success" onClick={handleSave}>
                  Save
                </Button>
              </div>
            </div>
          );
        })}
        {editing === null && (
          <Button
            className="mt-3"
            variant="Text"
            disabled={!isValid}
            onClick={addNewBookmark}
          >
            <FontAwesomeIcon icon="plus" className="mr-1" />
            Add bookmark
          </Button>
        )}
      </form>
    </>
  );
};

export default Form;
