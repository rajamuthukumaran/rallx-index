"use client";

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import SilentButton from "../../inputs/SilentButton";
import Dialog from "../../elements/Dialog";
import { BookmarkFormValue, BookmarkModificationProps, Editing } from "./types";
import Form from "./Form";

library.add(fas);

const BookmarkModification: React.FC<BookmarkModificationProps> = ({
  isDark,
  onSubmit: saveBookmarksToDB,
  bookmarks,
}) => {
  const [showDialog, setShowDialog] = useState(false);
  const [editing, setEditing] = useState<Editing>(null);
  const formObj = useForm<BookmarkFormValue>({
    defaultValues: {
      bookmarks: bookmarks || [],
    },
  });
  const formArray = useFieldArray({
    control: formObj.control,
    name: "bookmarks",
  });

  const onClose = useCallback(() => {
    if (!formObj.formState.isValid) {
      if (editing?.data) {
        formArray.update(editing.index, editing.data);
      } else {
        formArray.remove(editing?.index);
      }
    }
    !!editing && setEditing(null);
    setShowDialog(false);
  }, [formObj.formState.isValid]);

  const onSubmit = useCallback((value: BookmarkFormValue) => {
    saveBookmarksToDB && saveBookmarksToDB(value?.bookmarks);
  }, []);

  return (
    <div>
      <div className="fixed top-0 right-0 m-6">
        <SilentButton
          icon="pen"
          name="Add bookmark"
          size="md"
          onClick={() => setShowDialog(true)}
          isDark={isDark}
        />
      </div>
      <Dialog
        title="Bookmark Modification"
        showDialog={showDialog}
        onClose={onClose}
        cancel={{
          onClick: onClose,
        }}
        confirm={{
          onClick: () => formObj.handleSubmit(onSubmit)(),
        }}
      >
        <Form
          formObj={formObj}
          formArray={formArray}
          onSubmit={onSubmit}
          editing={editing}
          setEditing={setEditing}
        />
      </Dialog>
    </div>
  );
};

export default BookmarkModification;
