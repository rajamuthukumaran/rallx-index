import type { Meta, StoryFn } from "@storybook/react";
import { DialogProps } from "./types";
import Component from ".";

const meta: Meta<typeof Component> = {
  title: "Elements/Dialog",
  component: Component,
  tags: ["autodocs"],
  args: {
    title: "Test title",
  },
};

export default meta;

const Template: StoryFn = (args: DialogProps) => (
  <div>
    <Component {...args}>
      <div>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo
        similique modi in assumenda soluta, aliquam consectetur a quis. Qui quo
        mollitia vero? Dolorem at, ex et eveniet aliquam eius quidem reiciendis
        vel autem! Fugiat aliquam fuga quo soluta magnam? Labore at fugiat enim
        nesciunt quibusdam suscipit. Incidunt quod ipsam rem molestiae
        voluptatum, officiis temporibus id debitis accusamus totam natus
        tempore!
      </div>
    </Component>
  </div>
);

export const Default = Template.bind({});
