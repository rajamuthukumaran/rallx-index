import React from "react";
import clsx from "clsx";
import { omit } from "lodash";
import Button from "../../inputs/Button";
import { DialogProps } from "./types";

const customButtonProps = ["variant", "colorType", "name", "onClick"];

const Dialog: React.FC<DialogProps> = ({
  title,
  confirm,
  cancel,
  children,
  showDialog,
  onClose,
  className,
}) => {
  if (!showDialog) return null;

  return (
    <div
      className={clsx(
        "fixed inset-0 bg-black bg-opacity-75 z-20",
        "grid place-items-center"
      )}
      onClick={(e) => {
        e.stopPropagation();
        onClose && onClose();
      }}
    >
      <div
        className={clsx(
          "bg-gray-900 text-white",
          "m-6 rounded-3xl",
          "grid md:max-w-3xl md:min-w-[600px]",
          className
        )}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div className="text-2xl font-bold px-6 pt-6 pb-3">{title}</div>
        <div className="max-h-[calc(100vh-300px)] overflow-auto px-6 pb-3">
          {children}
        </div>
        <div className={clsx("flex justify-between p-6")}>
          <Button
            variant={cancel?.variant || "Text"}
            colorType={cancel?.colorType}
            onClick={(e) => {
              e.stopPropagation();
              cancel?.onClick && cancel?.onClick(e);
            }}
            {...omit(cancel || {}, customButtonProps)}
          >
            {cancel?.name || "Cancel"}
          </Button>
          <Button
            variant={confirm?.variant}
            colorType={confirm?.colorType}
            onClick={(e) => {
              e.stopPropagation();
              confirm?.onClick && confirm?.onClick(e);
            }}
            {...omit(confirm || {}, customButtonProps)}
          >
            {confirm?.name || "Okay"}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Dialog;
