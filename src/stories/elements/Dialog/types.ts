import { ButtonHTMLAttributes } from "react";
import { ButtonVarient, ColorType } from "@/stories/inputs/Button/types";
import { ReactChild } from "@/types/common";

export type DialogProps = ReactChild & {
  title?: string;
  confirm?: ButtonHTMLAttributes<HTMLButtonElement> & {
    name?: string;
    colorType?: ColorType;
    variant?: ButtonVarient;
  };
  cancel?: ButtonHTMLAttributes<HTMLButtonElement> & {
    name?: string;
    colorType?: ColorType;
    variant?: ButtonVarient;
  };
  showDialog?: boolean;
  onClose?: () => void;
  className?: string;
};
