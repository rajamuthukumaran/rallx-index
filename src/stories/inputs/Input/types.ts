import { InputHTMLAttributes } from "react";
import { InputColorTypes, InputVariants } from ".";

export type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  /**
   * Default: Underline
   */
  variant?: InputVariants;
  /**
   * Default: Primary
   */
  colorType?: InputColorTypes;
  error?: string;
  label?: string;
  isDark?: boolean;
};
