import clsx from "clsx";
import React from "react";
import { InputProps } from "./types";

const InputStyles = {
  Underline: "border-b-2",
  Outline: "border-2 rounded-lg",
};

const ColorStyles = {
  Primary: "hover:border-purple-600 focus:border-purple-800",
  Secondary: "hover:border-indigo-600 focus:border-indigo-800",
  Error: "hover:border-red-800 focus:border-red-600",
  Success: "hover:border-green-800 focus:border-green-600",
  Transparent: " hover:opacity-70 text-black",
};

export type InputVariants = keyof typeof InputStyles;
export type InputColorTypes = keyof typeof ColorStyles;

const Input: React.FC<InputProps> = ({
  className,
  variant = "Underline",
  colorType = "Primary",
  error,
  label,
  isDark,
  ...rest
}) => {
  return (
    <div className="inline-flex flex-col">
      {label && (
        <label
          className={clsx("font-bold", isDark ? "text-white" : "text-black")}
        >
          {label}
        </label>
      )}
      <input
        className={clsx(
          className,
          InputStyles[variant],
          ColorStyles[error ? "Error" : colorType],
          error && "border-red-600",
          isDark ? "text-white" : "text-black",
          "p-1 transition-colors focus:outline-none",
          "bg-transparent"
        )}
        {...rest}
      />
      {error && <span className="mt-1 text-red-600">{error}</span>}
    </div>
  );
};

export default Input;
