import React from "react";
import clsx from "clsx";
import { ButtonProps } from "./types";

const ButtonStyles = {
  Text: clsx("bg-transparent hover:opacity-70"),
  Pill: clsx("px-4 py-1 rounded-2xl"),
  Normal: clsx("px-4 py-1 rounded-md"),
};

const ColorStyles = {
  Primary: "bg-purple-800 hover:bg-purple-600 text-white",
  Secondary: "bg-indigo-800 hover:bg-indigo-600 text-white",
  Error: "bg-red-600 hover:bg-red-800 text-white",
  Success: "bg-green-600 hover:bg-green-800 text-white",
  Transparent: "bg-transparent hover:opacity-70 text-black",
};

const Button: React.FC<ButtonProps> = ({
  className,
  children,
  variant = "Normal",
  colorType = "Primary",
  ...rest
}) => {
  return (
    <button
      className={clsx(
        "transition-all font-semibold",
        colorType !== "Custom" && variant !== "Text" && ColorStyles[colorType],
        ButtonStyles[variant],
        className
      )}
      {...rest}
    >
      {children}
    </button>
  );
};

export default Button;
