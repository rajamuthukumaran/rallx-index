import type { Meta, StoryFn } from "@storybook/react";
import { ButtonProps } from "./types";
import Component from ".";

const meta: Meta<typeof Component> = {
  title: "Inputs/Button",
  component: Component,
  tags: ["autodocs"],
  args: {},
};

export default meta;

const Template: StoryFn = (args: ButtonProps) => (
  <div>
    <Component {...args}>Click me</Component>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  variant: "Normal",
  colorType: "Primary",
};
