import { ButtonHTMLAttributes } from "react";

export type ButtonVarient = "Text" | "Pill" | "Normal";
export type ColorType =
  | "Primary"
  | "Secondary"
  | "Error"
  | "Success"
  | "Transparent"
  | "Custom";

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  /**
   * Default: normal
   */
  variant?: ButtonVarient;
  /**
   * Default: Primary
   */
  colorType?: ColorType;
};
