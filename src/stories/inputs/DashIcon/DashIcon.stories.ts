import type { Meta, StoryObj } from "@storybook/react";
import Component from ".";

const meta: Meta<typeof Component> = {
  title: "Inputs/DashIcon",
  component: Component,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof Component>;

export const Default: Story = {
  args: {
    url: "https://www.google.com",
    name: "Settings",
    icon: "coffee",
    iconType: "fontawesome",
    openInNewTab: true,
    showName: true,
  },
};

export const CustomSize: Story = {
  args: {
    url: "https://www.google.com",
    name: "Settings",
    icon: "gear",
    iconType: "fontawesome",
    className: "w-12 h-12 bg-red-200 hover:bg-red-300 ",
    iconClassName: "w-6 h-6",
    customBg: true,
    customSize: true,
  },
};
