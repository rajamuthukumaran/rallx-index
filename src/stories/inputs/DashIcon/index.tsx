/* eslint-disable @next/next/no-img-element */
import React, { useMemo } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { DashIconProps } from "./type";
import { ReactChild } from "@/types/common";

const DashIcon: React.FC<DashIconProps & ReactChild> = ({
  icon,
  name,
  url,
  isDark,
  className,
  iconType,
  iconClassName,
  nameTagClassName,
  openInNewTab,
  customBg,
  customSize,
  showName,
}) => {
  const link = useMemo(() => {
    const hasProtocal =
      url?.startsWith("http://") || url?.startsWith("https://");
    return hasProtocal ? url : `http://${url}`;
  }, [url]);

  let RenderIcon;
  const iconClass = clsx(
    iconClassName ? iconClassName : "md:h-9 md:w-9 h-6 w-6",
    "text-white"
  );

  if (icon) {
    switch (iconType) {
      case "fontawesome":
        {
          RenderIcon = (
            <FontAwesomeIcon icon={icon as IconProp} className={iconClass} />
          );
        }
        break;
      case "custom":
        {
          RenderIcon = (
            <img src={icon as string} alt={name} className={iconClass} />
          );
        }
        break;
      default: {
        RenderIcon = <FontAwesomeIcon icon="gear" className={iconClass} />;
      }
    }
  } else {
    RenderIcon = <FontAwesomeIcon icon="gear" className={iconClass} />;
  }

  return (
    <div className="inline-flex flex-col justify-center items-center">
      <a
        title={name}
        href={link}
        target={openInNewTab ? "_blank" : undefined}
        rel="noopener noreferrer"
        className={clsx(
          "rounded-full transition-all",
          "inline-flex items-center justify-center",
          !customSize && "md:h-20 md:w-20  h-12 w-12",
          !customBg && "bg-gray-700 hover:bg-gray-600",
          className
        )}
      >
        {RenderIcon}
      </a>
      {showName && name && (
        <span
          className={clsx(
            "mt-1 text-xs md:text-sm font-semibold",
            isDark ? "text-white" : "text-black",
            "truncate text-ellipsis md:max-w-[8rem] max-w-[5rem]",
            nameTagClassName
          )}
        >
          {name}
        </span>
      )}
    </div>
  );
};

export default DashIcon;
