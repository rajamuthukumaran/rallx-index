import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { ButtonHTMLAttributes } from "react";
import { IconType } from "@/types/icons";

export type DashIconProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  icon?: IconProp | string;
  iconType?: IconType;
  name: string;
  url?: string;
  iconClassName?: string;
  nameTagClassName?: string;
  openInNewTab?: boolean;
  /**
   * Enable this if you are using custom size for the icon
   */
  customSize?: boolean;
  /**
   * Enable this if you are using custom background for the icon
   */
  customBg?: boolean;
  showName?: boolean;
  isDark?: boolean;
};
