import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import { SilentButtonProps } from "./type";

export const sizeToPx = {
  xs: "h-2 w-2",
  sm: "h-3 w-3",
  md: "h-4 w-4",
  lg: "h-6 w-6",
  xl: "h-8 w-8",
  sxl: "h-12 w-12",
  xxl: "h-16 w-16",
};

const SilentButton: React.FC<SilentButtonProps> = ({
  name,
  icon,
  isDark,
  size,
  iconClass,
  className,
  ...rest
}) => {
  return (
    <button
      className={clsx(className, size && sizeToPx[size])}
      title={name}
      {...rest}
    >
      <FontAwesomeIcon
        className={iconClass}
        icon={icon}
        color={isDark ? "#fff" : "#000"}
      />
    </button>
  );
};

export default SilentButton;
