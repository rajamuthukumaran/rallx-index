import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { ButtonHTMLAttributes } from "react";
import { sizeToPx } from ".";

export type SilentButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  name: string;
  icon: IconProp;
  size?: keyof typeof sizeToPx;
  isDark?: boolean;
  iconClass?: string;
};
