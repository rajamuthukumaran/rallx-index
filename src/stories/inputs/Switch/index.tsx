import clsx from "clsx";
import React from "react";
import { SwitchProps } from "./types";

export type SwitchColorTypes = keyof typeof ColorStyles;

const ColorStyles = {
  Neutral: clsx(
    "checked:bg-primary checked:after:bg-primary checked:focus:bg-primary",
    "bg-neutral-300 dark:bg-neutral-600 dark:after:bg-neutral-400",
    "dark:checked:after:bg-primary dark:checked:bg-primary",
    "dark:checked:focus:before:shadow-[3px_-1px_0px_13px_#3b71ca]"
  ),
  Primary: clsx(
    "checked:bg-purple-800 checked:after:bg-purple-800 checked:focus:bg-purple-800",
    "bg-purple-300 dark:bg-purple-200 dark:after:bg-purple-400",
    "dark:checked:after:bg-purple-600 dark:checked:bg-purple-800",
    "dark:checked:focus:before:shadow-[3px_-1px_0px_13px_#6b21a8]"
  ),
};

const Switch: React.FC<SwitchProps> = ({
  checked,
  className,
  onClick,
  label,
  colorType = "Primary",
}) => {
  return (
    <div className="inline-flex items-center">
      {label && <label className="mr-2">{label}</label>}
      <input
        className={clsx(
          "mr-2 mt-[0.3rem] h-3.5 w-8",
          "appearance-none rounded-[0.4375rem] hover:cursor-pointer",
          "before:pointer-events-none before:absolute before:h-3.5 before:w-3.5 before:rounded-full before:bg-transparent before:content-['']",
          "after:absolute after:z-[2] after:-mt-[0.1875rem] after:h-5 after:w-5 after:rounded-full after:border-none after:content-['']",
          "after:bg-neutral-100 after:shadow-[0_0px_3px_0_rgb(0_0_0_/_7%),_0_2px_2px_0_rgb(0_0_0_/_4%)] after:transition-[background-color_0.2s,transform_0.2s]",
          "checked:after:absolute checked:after:z-[2] checked:after:-mt-[3px] checked:after:ml-[1.0625rem] checked:after:h-5 checked:after:w-5 checked:after:rounded-full checked:after:border-none checked:after:content-['']",
          "checked:after:shadow-[0_3px_1px_-2px_rgba(0,0,0,0.2),_0_2px_2px_0_rgba(0,0,0,0.14),_0_1px_5px_0_rgba(0,0,0,0.12)] checked:after:transition-[background-color_0.2s,transform_0.2s]",
          "focus:outline-none focus:ring-0 focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[3px_-1px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-5 focus:after:w-5 focus:after:rounded-full focus:after:content-[''] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s]",
          "checked:focus:border-primary checked:focus:before:ml-[1.0625rem] checked:focus:before:scale-100 checked:focus:before:shadow-[3px_-1px_0px_13px_#3b71ca]",
          "dark:focus:before:shadow-[3px_-1px_0px_13px_rgba(255,255,255,0.4)]",
          ColorStyles[colorType],
          className
        )}
        type="checkbox"
        role="switch"
        id="flexSwitchCheckDefault02"
        checked={checked}
        onClick={onClick}
      />
    </div>
  );
};

export default Switch;
