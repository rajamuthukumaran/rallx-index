import { MouseEvent } from "react";
import { SwitchColorTypes } from ".";

export type SwitchProps = {
  checked?: boolean;
  /**
   * Default: Primary
   */
  colorType?: SwitchColorTypes;
  className?: string;
  label?: string;
  onClick?: (event: MouseEvent) => void;
};
