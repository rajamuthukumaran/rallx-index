import { InputHTMLAttributes } from "react";

export type RadioButtonProps = InputHTMLAttributes<HTMLInputElement> & {
  values: {
    label?: string;
    value?: string;
  }[];
  name: string;
};
