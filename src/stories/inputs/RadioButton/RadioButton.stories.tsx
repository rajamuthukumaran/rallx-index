import type { Meta, StoryFn } from "@storybook/react";
import { RadioButtonProps } from "./types";
import Component from ".";

const meta: Meta<typeof Component> = {
  title: "inputs/RadioButton",
  component: Component,
  tags: ["autodocs"],
  args: {
    name: "test",
    values: [
      {
        label: "Custom",
        value: "custom",
      },
      {
        label: "Fontawsome",
        value: "fontawesome",
      },
    ],
  },
};

export default meta;

const Template: StoryFn = (args) => (
  <div>
    <Component value="fontawesome" {...(args as RadioButtonProps)} />
  </div>
);

export const Default = Template.bind({});
