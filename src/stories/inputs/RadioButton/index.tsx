import React from "react";
import clsx from "clsx";
import { RadioButtonProps } from "./types";

const RadioButton: React.FC<RadioButtonProps> = ({
  className,
  values,
  value,
  ...rest
}) => {
  return (
    <div className={clsx("inline-flex items-center gap-4", className)}>
      {values?.map((data) => (
        <div key={data.value} className="inline-flex items-center">
          <input
            checked={value === data?.value}
            className="h-4 w-4"
            type="radio"
            value={data.value}
            {...rest}
          />
          {data?.label && (
            <label className="ml-1 font-semibold">{data.label}</label>
          )}
        </div>
      ))}
    </div>
  );
};

export default RadioButton;
