import React from "react";
import { Controller } from "react-hook-form";
import Input from "../inputs/Input";
import Switch from "../inputs/Switch";
import RadioButton from "../inputs/RadioButton";
import { FormInputProps, FormRadioButtonProps, FormSwitchProps } from "./types";

export const FormInput: React.FC<FormInputProps> = ({
  control,
  rules = {},
  name,
  fieldProps,
  required,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required: {
          message: `Required`,
          value: !!required,
        },
        ...rules,
      }}
      render={({ field, fieldState }) => (
        <Input {...field} {...fieldProps} error={fieldState.error?.message} />
      )}
    />
  );
};

export const FormSwitch: React.FC<FormSwitchProps> = ({
  control,
  rules = {},
  name,
  fieldProps,
  required,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
        ...rules,
      }}
      render={({ field }) => <Switch {...field} {...fieldProps} />}
    />
  );
};

export const FormRadioButton: React.FC<FormRadioButtonProps> = ({
  control,
  rules = {},
  name,
  fieldProps,
  required,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required,
        ...rules,
      }}
      render={({ field }) => <RadioButton {...field} {...fieldProps} />}
    />
  );
};
