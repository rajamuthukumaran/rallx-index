import { Control, RegisterOptions } from "react-hook-form";
import { InputProps } from "../inputs/Input/types";
import { SwitchProps } from "../inputs/Switch/types";
import { RadioButtonProps } from "../inputs/RadioButton/types";

export type CoreFormProps = {
  name: string;
  control: Control<any>;
  rules?: Omit<
    RegisterOptions<any, "checkbox">,
    "setValueAs" | "disabled" | "valueAsNumber" | "valueAsDate"
  >;
  required?: boolean;
  label?: string;
};

export type FormInputProps = CoreFormProps & {
  fieldProps?: Omit<InputProps, "value" | "onChange" | "name" | "error">;
};

export type FormSwitchProps = CoreFormProps & {
  fieldProps?: Omit<SwitchProps, "checked" | "onClick">;
};

export type FormRadioButtonProps = CoreFormProps & {
  fieldProps: Omit<RadioButtonProps, "onClick" | "name">;
};
