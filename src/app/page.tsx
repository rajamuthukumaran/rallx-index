import clsx from "clsx";
import { HOST_URL } from "../../host_config";
import Dock from "./Dock";
import { BookmarkRes } from "./api/bookmark/constants";
import ManageBookmark from "./ManageBookmark";

const getBookmarks = async () => {
  try {
    const res = await fetch(`${HOST_URL}/api/bookmark`, {
      method: "GET",
      cache: "no-store",
    });
    const resData: BookmarkRes = await res.json();
    if (resData?.data?.length) {
      return resData.data;
    }
  } catch (err) {
    console.error(err);
  }

  return [];
};

export default async function Home() {
  const bookmarks = await getBookmarks();

  return (
    <main
      className={clsx(
        "flex flex-col items-center justify-end relative",
        "py-8 px-8 min-h-screen",
        `bg-[url(https://wallpaperswide.com/download/house_island_2-wallpaper-3440x1440.jpg)] bg-cover bg-no-repeat`
      )}
    >
      <div>
        <Dock bookmarks={bookmarks} />
      </div>
      <ManageBookmark bookmarks={bookmarks} />
    </main>
  );
}
