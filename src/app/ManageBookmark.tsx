"use client";

import React from "react";
import { HOST_URL } from "../../host_config";
import { Bookmark } from "../../data/type";
import BookmarkModification from "@/stories/components/BookmarkModification";

const ManageBookmark: React.FC<{ bookmarks: Bookmark[] }> = ({ bookmarks }) => {
  const saveBookmarks = async (newBookmarks: Bookmark[]) => {
    const res = await fetch(`${HOST_URL}/api/bookmark`, {
      method: "PUT",
      body: JSON.stringify({
        newBookmarks,
      }),
    });

    if (res.status === 201) {
      window.location.reload();
    } else {
      console.error("Something went wrong, please try again");
    }
  };

  return (
    <BookmarkModification
      bookmarks={bookmarks}
      onSubmit={saveBookmarks}
      isDark
    />
  );
};

export default ManageBookmark;
