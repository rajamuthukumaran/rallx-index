import clsx from "clsx";
import { Bookmark } from "../../data/type";
import DashIcon from "@/stories/inputs/DashIcon";

const Dock: React.FC<{ bookmarks: Bookmark[] }> = ({ bookmarks }) => {
  return (
    <div
      className={clsx(
        "grid grid-cols-4 lg:grid-cols-8",
        "place-items-center gap-5"
      )}
    >
      {bookmarks.map((bookmark, index) => (
        <DashIcon
          key={index}
          icon={bookmark?.icon}
          url={bookmark.link}
          name={bookmark.name}
          iconType={bookmark.iconType}
          showName
        />
      ))}
    </div>
  );
};

export default Dock;
