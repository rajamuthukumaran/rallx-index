import "./globals.css";
import { Inter } from "next/font/google";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { HOST_URL } from "../../host_config";

library.add(fas);

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Rallx Index",
  description: "Server homepage",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <link rel="manifest" href="/manifest.json" />
        <link rel="apple-touch-icon" href="/icon.png"></link>
        <meta name="theme-color" content="#3E0055" />

        <meta name="application-name" content="Rallx Index" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta name="apple-mobile-web-app-title" content="Rallx Index" />
        <meta name="description" content="Dashboard for server" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        {/* <meta name="msapplication-config" content="/icons/browserconfig.xml" /> */}
        <meta name="msapplication-TileColor" content="#3E0055" />
        <meta name="msapplication-tap-highlight" content="no" />
        <meta name="theme-color" content="#000000" />

        <meta property="og:type" content="website" />
        <meta property="og:title" content="Rallx Index" />
        <meta property="og:description" content="Dashboard for server" />
        <meta property="og:site_name" content="Rallx Index" />
        <meta property="og:url" content={HOST_URL} />
        <meta property="og:image" content="/icon-512x512.png" />
      </head>
      <body className={inter.className}>{children}</body>
    </html>
  );
}
