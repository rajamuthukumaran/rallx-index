import { Bookmark } from "../../../../data/type";
import { RallxResponse } from "@/utils/responseGenerator";

export type BookmarkRes = RallxResponse<Bookmark[]>;
export type SaveBookmarkParams = {
  newBookmarks: Bookmark[];
};

export const bookmarksRequiredValues = ["name", "link"];
