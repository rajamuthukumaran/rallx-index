import { NextRequest } from "next/server";
import { SaveBookmarkParams, bookmarksRequiredValues } from "./constants";
import { readData, saveData } from "@/utils/jsonManager";
import { sendResponse } from "@/utils/responseGenerator";
import { validate } from "@/utils/validate";

export const GET = async () => {
  const data = await readData("bookmark");
  return sendResponse({
    data: data?.bookmarks,
  });
};

export const PATCH = async (req: NextRequest) => {
  try {
    const body: SaveBookmarkParams = await req.json();

    if (body?.newBookmarks?.length) {
      const isValid = body.newBookmarks.reduce((acc, bookmark) => {
        if (acc) {
          return validate(bookmark, bookmarksRequiredValues);
        }

        return acc;
      }, true);

      if (isValid) {
        const data = await readData("bookmark");
        const bookmarks = structuredClone(data?.bookmark || []);
        body.newBookmarks.forEach((bookmark) => {
          bookmarks.push(bookmark);
        });

        saveData("bookmark", {
          ...data,
          bookmarks,
        });

        return sendResponse({
          status: 201,
          data: bookmarks,
          message: "Bookmark saved successfully",
        });
      }
    }
  } catch (err) {
    console.error(err);
  }

  return sendResponse({
    error: "INVALID_REQUEST",
  });
};

export const PUT = async (req: NextRequest) => {
  try {
    const body: SaveBookmarkParams = await req.json();

    if (body?.newBookmarks?.length) {
      const isValid = body.newBookmarks.reduce((acc, bookmark) => {
        if (acc) {
          return validate(bookmark, bookmarksRequiredValues);
        }

        return acc;
      }, true);

      if (isValid) {
        const data = await readData("bookmark");
        const bookmarks = structuredClone(data?.bookmark || []);
        bookmarks.bookmark = [];
        body.newBookmarks.forEach((bookmark) => {
          bookmarks.push(bookmark);
        });

        saveData("bookmark", {
          ...data,
          bookmarks,
        });

        return sendResponse({
          status: 201,
          data: bookmarks,
          message: "Bookmark saved successfully",
        });
      }
    }
  } catch (err) {
    console.error(err);
  }

  return sendResponse({
    error: "INVALID_REQUEST",
  });
};
