import { SetStateAction, Dispatch, ReactNode } from "react";

export type StateSetter<t> = (value: t) => void | Dispatch<SetStateAction<t>>;

export type ReactChild = { children?: ReactNode };

export type NumChar = number | string;
