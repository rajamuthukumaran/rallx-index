import { IconType } from "@/types/icons";

export type DataFiles = "bookmark" | "config";

export type Bookmark = {
  name: string;
  link: string;
  icon?: string;
  iconType?: IconType;
  background?: string;
};

export type Config = {
  dashboard: {
    iconsSize?: number;
    defaultIconBackground?: string;
    background?: string;
  };
  apis: Record<string, string>;
};
