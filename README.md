A simple dashboard for your home lab

## Getting Started

Setup nodejs in your server. If you are using Proxmox, you can create a new LCX container (If you don't know how to do it folow the steps mentioend in **Setting up node server** below) Then run following commands

```bash
npm run build
npm run start
```

Now you can access Rallx index in **{your_server_ip}:3000**

## Setting up node server

For Ubuntu & Debian servers,

```bash
apt update && apt upgrade
apt install nodejs
```

For Fedora, Redhat and other RPM servers,

```bash
 sudo dnf clean all
 sudo dnf check-update
 sudo dnf update
 sudo dnf install nodejs
```


**THIS IS JUST A ALPHA VERSION MORE FEATURE COMMING SOON** 

## Things in progress
- Widgets for various services like Proxmox and Pihole
- Change Wallpaper and select dark vs light mode